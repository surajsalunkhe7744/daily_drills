const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interest: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}




//Q1 Find all users who are interested in playing video games.

function userInterestedVideoGame(users)
{
    const keys = Object.keys(users);
    let user = keys.map(userIndex =>{
            let tempUser = users[userIndex];
            //console.log(tempUser);
            let userInterest = tempUser.interests;
            let flag = false;
                    let = userInterest.map(interestIndex =>{
                        if(interestIndex.includes('Video Games')){
                                flag = true;
                        }
                    });
            if(flag === true)
            return tempUser;
            else
            return 0;
    });
    let result = user.filter(resIndex =>{
        if(resIndex != 0)
        return resIndex;
    });
    return result;
}


//Q2 Find all users staying in Germany.

function userInterestedVideoGame(users)
{
    const keys = Object.keys(users);
    let user = keys.map(userIndex =>{
            let userDetails = [];
            let tempUser = users[userIndex];
            //console.log(tempUser);
            let userCountry = tempUser.nationality;
            if(userCountry.includes("Germany")){
                userDetails.push(tempUser);
            }
        return userDetails;    
    });
    let result = user.filter(resIndex =>{
        if(resIndex.length != 0){
        let temp = resIndex[0];
        return temp;
        }
    })
    return result;
}

/*
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
*/

function masterUser(users)
{
    let userDetails = Object.values(users);
    let result = [];

    //Senior Sorted Data by designation and age.
    let senior = userDetails.filter(index =>{
        if(index.desgination.includes("Senior")){
            index["Senior"] = index.age;
            return index;
        }
    })
    let sortedSeniorAge = senior.sort((a,b) =>
        a.Senior > b.Senior ? -1:1
    );
    //console.log(sortedSeniorAge);
    result = result.concat(sortedSeniorAge);

    let developer = userDetails.filter(Index =>{
        if(Index.desgination.includes("Developer") && (!Index.desgination.includes("Senior"))){
            Index["Developer"] = Index.age;
            return Index;
        }
    })
    let sortedDeveloperAge = developer.sort((a,b) =>
        a.Developer > b.Developer ? -1:1
    );
    //console.log(sortedSeniorAge);
    result = result.concat(sortedDeveloperAge);

    let intern = userDetails.filter(Index =>{
        if(Index.desgination.includes("Intern")){
            //console.log(Index);
            Index["Intern"] = Index.age;
            //console.log(Index);
            return Index;
        }
    })
    //console.log(intern);
    let sortedInternAge = intern.sort((a,b) =>
        a.Intern > b.Intern ? -1:1
    );
    
    return result= result.concat(sortedInternAge);
}



//Q4 Find all users with masters Degree.

function masterUser(users)
{
    const keys = Object.keys(users);
    let user = keys.map(userIndex =>{
            let userDetails = [];
            let tempUser = users[userIndex];
            //console.log(tempUser);
            let userCountry = tempUser.qualification;
            if(userCountry == "Masters"){
                userDetails.push(tempUser);
            }
            return userDetails;
    });
    let result = user.filter(resIndex =>{
        if(resIndex.length != 0){
        let temp = resIndex[0];
        return temp;
        }
    })
    return result;
}


//Q5 Group users based on their Programming language mentioned in their designation.



//NOTE: Do not change the name of this file

