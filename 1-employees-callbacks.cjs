/*
* Use asynchronous callbacks ONLY wherever possible.
* Error first callbacks must be used always.
* Each question's output has to be stored in a json file.
* Each output file has to be separate.

* Ensure that error handling is well tested.
* After one question is solved, only then must the next one be executed. 
* If there is an error at any point, the subsequent solutions must not get executed.
   
* Store the given data into data.json
* Read the data from data.json
* Perfom the following operations.

*/

/*
1. Retrieve data for ids : [2, 13, 23].
2. Group data based on companies.
 { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
3. Get all data for company Powerpuff Brigade
4. Remove entry with id 2.
5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
6. Swap position of companies with id 93 and id 92.
7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

NOTE: Do not change the name of this file

*/

{
    "employees": [
        {
            "id": 23,
            "name": "Daphny",
            "company": "Scooby Doo"
        },
        {
            "id": 73,
            "name": "Buttercup",
            "company": "Powerpuff Brigade"
        },
        {
            "id": 93,
            "name": "Blossom",
            "company": "Powerpuff Brigade"
        },
        {
            "id": 13,
            "name": "Fred",
            "company": "Scooby Doo"
        },
        {
            "id": 89,
            "name": "Welma",
            "company": "Scooby Doo"
        },
        {
            "id": 92,
            "name": "Charles Xavier",
            "company": "X-Men"
        },
        {
            "id": 94,
            "name": "Bubbles",
            "company": "Powerpuff Brigade"
        },
        {
            "id": 2,
            "name": "Xyclops",
            "company": "X-Men"
        }
    ]
}


const fsp = require("fs");

fsp.readFile("./data.json", "utf-8", (err, object) => {
  if (err) {
    console.error("Error while reading file data.json");
  } else {
    let data = JSON.parse(object);
    let companyData = data.employees;
    let result = companyData.filter((company) => {
      if (company.id === 2 || company.id === 13 || company.id === 23)
        return company;
    });
    fsp.writeFile("./result1.json", JSON.stringify(result), (err) => {
      if (err) {
        console.error("Error while creating result1 file");
      } else {
        console.log("result1 file created successfully");
        fsp.readFile("./data.json", "utf-8", (err, object) => {
          if (err) {
            console.error("Error while reading file data.json");
          } else {
            let data = JSON.parse(object);
            let companyData = data.employees;
            let scooby_doo = [],
              powerpuff_brigade = [],
              x_men = [];

            let scooby_doo_Data = companyData.filter((employee) => {
              if (employee.company === "Scooby Doo") return employee;
            });
            let scooby = scooby_doo.concat(scooby_doo_Data);

            let powerpuff_brigade_Data = companyData.filter((employee) => {
              if (employee.company === "Powerpuff Brigade") return employee;
            });
            let powerpuff = powerpuff_brigade.concat(powerpuff_brigade_Data);

            let x_men_Data = companyData.filter((employee) => {
              if (employee.company === "X-Men") return employee;
            });
            let x_men_details = x_men.concat(x_men_Data);

            let obj = {
              "Scooby Doo": scooby,
              "Powerpuff Brigade": powerpuff,
              "X-Men": x_men_details,
            };

            fsp.writeFile("./result2.json", JSON.stringify(obj), (err) => {
              if (err) {
                console.error("Error while creating file result2");
              } else {
                console.log("result2 file created successfully");
                fsp.readFile("./data.json", "utf-8", (err, object) => {
                  if (err) {
                    console.error("Error while reading file data.json");
                  } else {
                    let data = JSON.parse(object);
                    let companyData = data.employees;
                    let result = companyData.filter((employee) => {
                      if (employee.company === "Powerpuff Brigade")
                        return employee;
                    });
                    fsp.writeFile(
                      "./result3.json",
                      JSON.stringify(result),
                      (err) => {
                        if (err) {
                          console.error("Error while creating result1 file");
                        } else {
                          console.log("result3 file created successfully");
                          fsp.readFile(
                            "./data.json",
                            "utf-8",
                            (err, object) => {
                              if (err) {
                                console.error(
                                  "Error while reading file data.json"
                                );
                              } else {
                                let data = JSON.parse(object);
                                let companyData = data.employees;
                                let result = companyData.filter((employee) => {
                                  if (employee.id !== 2) return employee;
                                });
                                fsp.writeFile(
                                  "./result4.json",
                                  JSON.stringify(result),
                                  (err) => {
                                    if (err) {
                                      console.error(
                                        "Error while creating result1 file"
                                      );
                                    } else {
                                      console.log(
                                        "result4 file created successfully"
                                      );
                                      fsp.readFile(
                                        "./data.json",
                                        "utf-8",
                                        (err, object) => {
                                          if (err) {
                                            console.error(
                                              "Error while reading file data.json"
                                            );
                                          } else {
                                            let data = JSON.parse(object);
                                            let companyData = data.employees;
                                            let filterPowerpuff =
                                              companyData.filter(
                                                (data) =>
                                                  data.company ===
                                                  "Powerpuff Brigade"
                                              );
                                            let powerpuff =
                                              sorttedData(filterPowerpuff);
                                            let filterScoobyDoo =
                                              companyData.filter(
                                                (data) =>
                                                  data.company === "Scooby Doo"
                                              );
                                            let scoobyDoo =
                                              sorttedData(filterScoobyDoo);
                                            let filterX_Men =
                                              companyData.filter(
                                                (data) =>
                                                  data.company === "X-Men"
                                              );
                                            let x_Men =
                                              sorttedData(filterX_Men);
                                            function sorttedData(companyData) {
                                              let result = companyData.sort(
                                                (a, b) => {
                                                  if (a.id > b.id) {
                                                    return 1;
                                                  } else {
                                                    return -1;
                                                  }
                                                }
                                              );
                                              return result;
                                            }
                                            let result = powerpuff.concat(
                                              scoobyDoo,
                                              x_Men
                                            );
                                            fsp.writeFile(
                                              "./result5.json",
                                              JSON.stringify(result),
                                              (err) => {
                                                if (err) {
                                                  console.error(
                                                    "Error while creating result5 file"
                                                  );
                                                } else {
                                                  console.log(
                                                    "result5 file created successfully"
                                                  );
                                                  fsp.readFile(
                                                    "./data.json",
                                                    "utf-8",
                                                    (err, object) => {
                                                      if (err) {
                                                        console.error(
                                                          "Error while reading file data.json"
                                                        );
                                                      } else {
                                                        let data =
                                                          JSON.parse(object);
                                                        let companyData =
                                                          data.employees;
                                                        let idData =
                                                          companyData.filter(
                                                            (company) =>
                                                              company.id ===
                                                                92 ||
                                                              company.id === 93
                                                          );
                                                        let temp =
                                                          idData[0].company;
                                                        idData[0].company =
                                                          idData[1].company;
                                                        idData[1].company =
                                                          temp;
                                                        let result = idData;
                                                        fsp.writeFile(
                                                          "./result6.json",
                                                          JSON.stringify(
                                                            result
                                                          ),
                                                          (err) => {
                                                            if (err) {
                                                              console.error(
                                                                "Error while creating result5 file"
                                                              );
                                                            } else {
                                                              console.log(
                                                                "result6 file created successfully"
                                                              );
                                                              fsp.readFile(
                                                                "./data.json",
                                                                "utf-8",
                                                                (
                                                                  err,
                                                                  object
                                                                ) => {
                                                                  if (err) {
                                                                    console.error(
                                                                      "Error while reading file data.json"
                                                                    );
                                                                  } else {
                                                                    let data =
                                                                      JSON.parse(
                                                                        object
                                                                      );
                                                                    let companyData =
                                                                      data.employees;
                                                                    let result =
                                                                      companyData.map(
                                                                        (
                                                                          company
                                                                        ) => {
                                                                          let id =
                                                                            company.id;
                                                                          if (
                                                                            id %
                                                                              2 ===
                                                                            0
                                                                          ) {
                                                                            company[
                                                                              "birthday"
                                                                            ] =
                                                                              new Date().toString();
                                                                          }
                                                                          return company;
                                                                        }
                                                                      );
                                                                    fsp.writeFile(
                                                                      "./result7.json",
                                                                      JSON.stringify(
                                                                        result
                                                                      ),
                                                                      (err) => {
                                                                        if (
                                                                          err
                                                                        ) {
                                                                          console.error(
                                                                            "Error while creating result5 file"
                                                                          );
                                                                        } else {
                                                                          console.log(
                                                                            "result7 file created successfully"
                                                                          );
                                                                        }
                                                                      }
                                                                    );
                                                                  }
                                                                }
                                                              );
                                                            }
                                                          }
                                                        );
                                                      }
                                                    }
                                                  );
                                                }
                                              }
                                            );
                                          }
                                        }
                                      );
                                    }
                                  }
                                );
                              }
                            }
                          );
                        }
                      }
                    );
                  }
                });
              }
            });
          }
        });
      }
    });
  }
});
