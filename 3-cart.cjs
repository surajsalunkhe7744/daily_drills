const products = [{
    shampoo: {
        price: "$50",
        quantity: 4
    },
    "Hair-oil": {
        price: "$40",
        quantity: 2,
        sealed: true
    },
    comb: {
        price: "$12",
        quantity: 1
    },
    utensils: [
        {
            spoons: { quantity: 2, price: "$8" }
        }, {
            glasses: { quantity: 1, price: "$70", type: "fragile" }
        },{
            cooker: { quantity: 4, price: "$900" }
        }
    ],
    watch: {
        price: "$800",
        quantity: 1,
        type: "fragile"
    }
}]



//Q1. Find all the items with price more than $65.

function itemPriceMoreThan65(products){
  let utensilsDetails = products[0].utensils;

  let utensilProducts = utensilsDetails.map((product) =>{
  return Object.entries(product).flat();
  })

  let productDetails = utensilProducts.reduce((acc,element) =>{
      acc[element[0]] = element[1];
      return acc;
  },[])
  
  let obj = Object.assign(products[0],productDetails);
  
  delete obj.utensils;
  let pricesValues = Object.values(obj);
  let pricesKeys = Object.keys(obj);

  let result = pricesKeys.reduce((acc,product,index) =>{
    let price = Number(pricesValues[index].price.substr(1));
    if(price > 65)
    acc[product] = pricesValues[index];
    return acc;
  },{});

  return result;
}


//Q2. Find all the items where quantity ordered is more than 1.

function itemOrderQuantityMoreThan1(products){
  let utensilsDetails = products[0].utensils;

  let utensilProducts = utensilsDetails.map((product) =>{
  return Object.entries(product).flat();
  })

  let reducer = utensilProducts.reduce((acc,element) =>{
      acc[element[0]] = element[1];
      return acc;
  },[])
  
  let obj = Object.assign(products[0],reducer);
  
  delete obj.utensils;
  let quantityValues = Object.values(obj);
  let quantityKeys = Object.keys(obj);

  let result = quantityKeys.reduce((acc,product,index) =>{
    let quantity = quantityValues[index].quantity;
    if(quantity > 1)
    acc[product] = quantityValues[index];
    return acc;
  },{});

  return result;
}

//Q.3 Get all items which are mentioned as fragile.

function fragileTypeItems(products){
  let utensilsDetails = products[0].utensils;

  let utensilProducts = utensilsDetails.map((product) =>{
  return Object.entries(product).flat();
  })

  let reducer = utensilProducts.reduce((acc,element) =>{
      acc[element[0]] = element[1];
      return acc;
  },[])
  
  let obj = Object.assign(products[0],reducer);
  
  delete obj.utensils;
  let productValues = Object.values(obj);
  let productKeys = Object.keys(obj);

  let result = productKeys.reduce((acc,product,index) =>{
    let productType = productValues[index].type;
    if(productType === "fragile")
    acc[product] = productValues[index];
    return acc;
  },{});

  return result;
}


//Q.4 Find the least and the most expensive item for a single quantity.


function fragileTypeItems(products){
  let utensilsDetails = products[0].utensils;

  let utensilProducts = utensilsDetails.map((product) =>{
  return Object.entries(product).flat();
  })

  let reducer = utensilProducts.reduce((acc,element) =>{
      acc[element[0]] = element[1];
      return acc;
  },[])
  
  let obj = Object.assign(products[0],reducer);
  
  delete obj.utensils;
  let productValues = Object.values(obj);
  let productKeys = Object.keys(obj);

  let addingPricePerQtyKey = productKeys.reduce((acc,product,index) =>{
    let price = Number(productValues[index].price.substr(1));
    let quantity = productValues[index].quantity;
    acc[product] = Object.assign(productValues[index],{["price_per_product"]:(price/quantity),['name']:product});
    return acc;
  },{});

  let updatedProductsValues = Object.values(addingPricePerQtyKey);

  //console.log(updatedProductsValues);

  let sortedData = updatedProductsValues.sort((a,b) =>{
    if(a.price_per_product>b.price_per_product){
      return -1;
    }else{
      return 1;
    }
  });

  let dataOfProduct = sortedData.reduce((acc,product,index) =>{
        if(index === 0 || index === sortedData.length-1)
          acc[product.name] = product;
          return acc;
  },[]);

  return dataOfProduct;
}


//Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

function groupProductByState(products){
  let utensilsDetails = products[0].utensils;

  let utensilProducts = utensilsDetails.map((product) =>{
  return Object.entries(product).flat();
  })

  let reducer = utensilProducts.reduce((acc,element) =>{
      acc[element[0]] = element[1];
      return acc;
  },[])
  
  let obj = Object.assign(products[0],reducer);
  
  delete obj.utensils;

  let result ={
    ['solid'] : {
      ["comb"] :obj.comb,
      ["watch"] :obj.watch,
      ["spoons"] : obj.spoons,
      ["glasses"] :obj.glasses,
      ["cooker"] :obj.cooker
    },
    ['liquid'] :{
      ["shampoo"]:obj.shampoo,
      ["Hair-oil"]:obj["Hair-oil"]
    },
    ['gas']:{}
  }
  return result;
}

//NOTE: Do not change the name of this file

