/*
NOTE: Do not change the name of this file

* Ensure that error handling is well tested.
* If there is an error at any point, the subsequent solutions must not get executed.
* Solutions without error handling will get rejected and you will be marked as having not completed this drill.

* Usage of async and await is not allowed.

Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos

Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

Using promises and the `fetch` library, do the following. 

*/

// 1. Fetch all the users

function fetchUsersData(){
fetch("https://jsonplaceholder.typicode.com/users")
.then((data) => data.json())
.then((users) =>{
    console.log(users);
})
}

//fetchUsersData();

// 2. Fetch all the todos

function fetchTodosData(){
fetch("https://jsonplaceholder.typicode.com/todos")
.then((data) => data.json())
.then((todos) =>{
    console.log(todos);
})
}

//fetchTodosData();

// 3. Use the promise chain and fetch the users first and then the todos.
function fetchFirstData(){
fetch("https://jsonplaceholder.typicode.com/users")
.then((data) => data.json())
.then((users) =>{
    console.log(users[0]);
    return fetch("https://jsonplaceholder.typicode.com/todos")
})
.then((data) => data.json())
.then((todos) =>{
    console.log(todos[0]);
})
.catch((err) =>{
    console.error(err);
})
}

//fetchFirstData();

// 4. Use the promise chain and fetch the users first and then all the details for each user.

function fetchData(){
fetch("https://jsonplaceholder.typicode.com/users")
.then((data) => data.json())
.then((users) =>{
    for(let user = 0; user < users.length; user++){
        userDetails(users[user])
    }
})
.catch((err) =>{
    console.error(err);
})
}

function userDetails(user){
    fetch("https://jsonplaceholder.typicode.com/todos")
    .then((data) => data.json())
    .then((todos) =>{
        console.log("Details of user ID : "+user.id);
        let result = todos.filter((todo) =>{
            if(todo.userId === user.id)
            return todo;
        })
        console.log(result);
    })
    .catch((err) =>{
        console.error(err);
    })
}

//fetchData();

// 5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo

function fetchTodoUserData(){
    fetch("https://jsonplaceholder.typicode.com/todos")
    .then((data) => data.json())
    .then((todoList) =>{
            userDetails(todoList[0])
    })
    .catch((err) =>{
        console.error(err);
    })
    }
    
    function userDetails(todo){
        fetch("https://jsonplaceholder.typicode.com/users")
        .then((data) => data.json())
        .then((users) =>{
            console.log("Details of todo ID : "+todo.id);
            let result = users.filter((user) =>{
                if(todo.id === user.id)
                return user;
            })
            console.log(result);
        })
        .catch((err) =>{
            console.error(err);
        })
    }
    
    //fetchTodoUserData();


// NOTE: If you need to install `node-fetch` or a similar libary, let your mentor know before doing so along with the reason why. No other exteral libraries are allowed to be used.

// Usage of the path libary is recommended



