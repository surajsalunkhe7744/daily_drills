[{"id":1,"first_name":"Valera","last_name":"Pinsent","email":"vpinsent0@google.co.jp","gender":"Male","ip_address":"253.171.63.171"},
{"id":2,"first_name":"Kenneth","last_name":"Hinemoor","email":"khinemoor1@yellowbook.com","gender":"Polygender","ip_address":"50.231.58.150"},
{"id":3,"first_name":"Roman","last_name":"Sedcole","email":"rsedcole2@addtoany.com","gender":"Genderqueer","ip_address":"236.52.184.83"},
{"id":4,"first_name":"Lind","last_name":"Ladyman","email":"lladyman3@wordpress.org","gender":"Male","ip_address":"118.12.213.144"},
{"id":5,"first_name":"Jocelyne","last_name":"Casse","email":"jcasse4@ehow.com","gender":"Agender","ip_address":"176.202.254.113"},
{"id":6,"first_name":"Stafford","last_name":"Dandy","email":"sdandy5@exblog.jp","gender":"Female","ip_address":"111.139.161.143"},
{"id":7,"first_name":"Jeramey","last_name":"Sweetsur","email":"jsweetsur6@youtube.com","gender":"Genderqueer","ip_address":"196.247.246.106"},
{"id":8,"first_name":"Anna-diane","last_name":"Wingar","email":"awingar7@auda.org.au","gender":"Agender","ip_address":"148.229.65.98"},
{"id":9,"first_name":"Cherianne","last_name":"Rantoul","email":"crantoul8@craigslist.org","gender":"Genderfluid","ip_address":"141.40.134.234"},
{"id":10,"first_name":"Nico","last_name":"Dunstall","email":"ndunstall9@technorati.com","gender":"Female","ip_address":"37.12.213.144"}]

 

    //1. Find all people who are Agender
    
    function peopleAgender(personalDetails)
   {
    let result = [];
    const people = personalDetails.map(pDetail =>
        {
           result.push(pDetail);   
        })
    return result;
   }



    //2. Split their IP address into their components eg. 111.139.161.143 has components [111, 139, 161, 143]
    
    function ipAddressToArray(personalDetails)
   {
    let result = [];
    const people = personalDetails.map(pDetail =>
        {
            let ipTemp = pDetail.ip_address
            let ipTempArray = ipTemp.split(".");
            result.push(ipTempArray);
        })
    return result;
    }
    
    
    //3. Find the sum of all the second components of the ip addresses.
    
    function sumIPAdressOfSecondPosition(personalDetails)
    {
    let result = [];
    const people = personalDetails.map(pDetail =>
        {
            let ipTemp = pDetail.ip_address
            let ipTempArray = ipTemp.split(".");
            result.push(ipTempArray);
        })
    let add = 0;
    const sum = result.map(ipdetails =>
        {
            add = Number(ipdetails[1]) + add;
        })
    return add;
    }


    //4. Find the sum of all the fourth components of the ip addresses
    
    function sumIPAdressOfFourthPosition(personalDetails)
    {
    let result = [];
    const people = personalDetails.map(pDetail =>
        {
            let ipTemp = pDetail.ip_address
            let ipTempArray = ipTemp.split(".");
            result.push(ipTempArray);
        })
    let add = 0;
    const sum = result.map(ipdetails =>
        {
            add = Number(ipdetails[3]) + add;
        })
    return add;
    }
    
    
    //5. Compute the full name of each person and store it in a new key (full_name or something) for each person.
    
    function personFullName(personalDetails)
    {
    let result = [];
    const people = personalDetails.map(pDetail =>
        {
            let tempName = pDetail.first_name + " " + pDetail.last_name;
            let obj = {"user_full_name" : tempName};
            result.push(obj);
        })
    
        return result;
    }
    
    //6. Filter out all the .org emails
    
    function allDotOrgEmails(personalDetails)
   {
    let result = [];
    const people = personalDetails.map(pDetail =>
        {
            let tempDomain = ".org";
            let tempPeopleDomain = pDetail.email;
            let peopleDomain = tempPeopleDomain.substr(tempPeopleDomain.length-4);
            if(tempDomain == peopleDomain)
            result.push(pDetail);
        })
    
        return result;
    }
    
    
    //7. Calculate how many .org, .au, .com emails are there
    
    function emailDomainCount(personalDetails)
    {
    let result = [];
    let orgCounter = 0,comCounter = 0,auCounter = 0;
    const people = personalDetails.map(pDetail =>
        {
            let tempDomain1 = ".org",tempDomain2 = ".com",tempDomain3 = ".au";
    
            let tempPeopleDomain = pDetail.email;
            let peopleDomain = tempPeopleDomain.substr(tempPeopleDomain.length-4);
            let auPeopleDomain = tempPeopleDomain.substr(tempPeopleDomain.length-3);
            if(tempDomain1 == peopleDomain)
            {
                orgCounter++;
            }
            else if(tempDomain2 == peopleDomain)
            {
                comCounter++;
            }
            else if(tempDomain3 == auPeopleDomain)
            {
                auCounter++;
            }
        })

        result.push({".org counter" : orgCounter});
        result.push({".com counter" : comCounter});
        result.push({".au counter" : auCounter});
        return result;
    }
    
    
    //8. Sort the data in descending order of first name
    
    function firstNameSort(personalDetails)
    {
    let result = [];
    let orgCounter = 0,comCounter = 0,auCounter = 0;
    const people = personalDetails.map(pDetail =>
        {
            result.push(pDetail.first_name);
        })
        result.sort().reverse();
        return result;
    }
    
    

    //NOTE: Do not change the name of this file

