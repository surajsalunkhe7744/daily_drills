const fs = require("fs");

/*
NOTE: Do not change the name of this file

NOTE: For all file operations use promises with fs. Promisify the fs callbacks rather than use fs/promises.

Q1. Create 2 files simultaneously (without chaining).
Wait for 2 seconds and starts deleting them one after another. (in order)
(Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)
*/

function fileOperations(){
    let promise = new Promise((resolve,reject) =>{
        fs.writeFile("file1.json",JSON.stringify({a:1,b:1,c:1}),(err) =>{
            if(err){
                reject("Error while creating file - file1.json");
            }else{
                fs.writeFile("file2.json",JSON.stringify({a:1,b:1,c:1}),(err) =>{
                    if(err){
                        console.error("Error while creating file - file2.json");
                    }
                    });
                resolve("Files created Successfully");
            }
        })
    });

    promise
    .then((data) => {
        console.log(data);
        console.log("Started deleting files, It will wait for 2 sec first");
        setTimeout(function(){
            fs.unlink("file1.json",(err) =>{
                if(err){
                    console.error("Error while deleting file - files1.json");
                }else{
                    fs.unlink("file2.json",(err) =>{
                        if(err){
                            console.error("Error while deleting file - files2.json");
                        }
                        console.log("Files deleted successfully");
                    })
                }
            })
        },2000);
    })
    .catch((err) => console.log(err));
}

//fileOperations();

/*
Q2. Create a new file with lipsum data (you can google and get this). 
Do File Read and write data to another file
Delete the original file 
Using promise chaining
*/

function fileOperations(){
    let data = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam quis elit nisl. Aliquam congue ut orci quis gravida. Etiam eleifend ultrices turpis, ut gravida ligula accumsan vel. Cras sagittis ex ac rutrum congue. Curabitur tristique nibh posuere nisi varius, non ullamcorper sapien venenatis. Maecenas non suscipit ex. Etiam commodo magna ullamcorper ex dignissim, sit amet lacinia dolor molestie. Morbi vitae scelerisque magna. Phasellus pulvinar, mauris id ornare consequat, elit sem fermentum nisl, eget varius sapien risus at felis.";
    let promise = new Promise((resolve,reject) =>{
        fs.writeFile("lipsum.txt",data,(err) =>{
            if(err){
                reject("Error while creating lipsum.txt file "+err);
            }else{
                resolve("file created successfully - lipsum.txt");
            }
        })
    })

    promise
    .then((data) =>{
        console.log(data);
        return new Promise((resolve,reject) => {
            fs.readFile("./lipsum.txt","utf-8",(err,data) =>{
                if(err){
                    reject("Error while reading file - lipsum.txt "+err);
                }else{
                    console.log("File readed successfully - lipsum.txt");
                    resolve(data);
                }
            })
        })
    })
    .then((data) =>{
        return new Promise((resolve,reject) =>{
            fs.writeFile("newLipsum.txt",data,(err) => {
                if(err){
                    reject("Error while creating newLipsum.txt file "+err);
                }else{
                    resolve("file created successfully - newLipsum.txt")
                }
            })
        })
    })
    .then((data) =>{
        console.log(data);
        fs.unlink("./lipsum.txt",(err) =>{
            if(err){
                console.error("Error while deleting file - lipsum.txt "+err);
            }else{
                console.log("Original file deleted successfully - lipsum.txt");
            }
        });
    })
    .catch((err) => console.log(err));
}

//fileOperations();


/*
Q3.
Use appropriate methods to 
A. login with value 3 and call getData once login is successful
B. Write logic to logData after each activity for each user. Following are the list of activities
    "Login Success"
    "Login Failure"
    "GetData Success"
    "GetData Failure"

    Call log data function after each activity to log that activity in the file.
    All logged activity must also include the timestamp for that activity.
    
    You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
    All calls must be chained unless mentioned otherwise.
    
*/


function login(user, val) {
    if(val % 2 === 0) {
        return Promise.resolve(user);
    } else {
        return Promise.reject(new Error("User not found"));
    }
}

function getData() {
    return Promise.resolve([
        {
            id: 1, 
            name: "Test",
        },
        {
            id: 2, 
            name: "Test 2",
        }
    ]);
}

function logData(user, activity) {
    // use promises and fs to save activity in some file
    fs.appendFile("logData.txt",user+" "+activity+"\n",(err) =>{
        if(err){
            console.error(err);
        }else{
            console.log("Log data updated successfully");
        }
    })
}

function loginAndGetData(){
    
    let userID = Math.floor(Math.random() * 10 + 1);
    console.log(userID);
    login("User_"+userID, userID)
    .then(() =>{
        return getData().then(() =>{
            logData(" "+"[User_"+userID+"] =","{Login Success, "+"GetData Success, "+new Date()+"}");
        });
    })
    .catch(() =>{
        logData(" "+"[User_"+userID+"] =","{Login Failure, "+"GetData Failure, "+new Date()+"}");
    })
}

loginAndGetData();
