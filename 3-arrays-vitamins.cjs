const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];



    //1. Get all items that are available 
    
    function availableItems(items)
    {
    let result = [];

    let availability = items.map(itemIndex => 
        {
            if(itemIndex.available === true)
            {
                result.push(itemIndex);
            }
        });

    return result;
    }
    
    
    //2. Get all items containing only Vitamin C.
    
    function containVitaminAOnly(items)
    {
    let result = [];

    let availability = items.map(itemIndex => 
        {
            if(itemIndex.contains == "Vitamin A")
            {
                result.push(itemIndex);
            }
        })

    return result;
    }
    
    
    //3. Get all items containing Vitamin A.
    
    function containVitaminA(items)
    {
    let result = [];

    let availability = items.map(itemIndex => 
        {
            if(itemIndex.contains.includes("Vitamin A"))
            {
                result.push(itemIndex);
            }
        })

    return result;
   }
    
    
    /* 4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.
    */
    
    
    function containVitaminA(items)
    {
    let result = [];
    let vitamins = {};
    let vit = [];

    let availability = items.map(itemIndex => 
        {
            let strArray = itemIndex.contains.split(",");
            //console.log(strArray);
            
            let strArrayPush = strArray.filter(strIndex =>
                {
                    let tempString = strIndex.trim();
                    if(!(vitamins.hasOwnProperty(tempString)))
                    {
                        vitamins[strIndex] = [];
                        vit.push(strIndex);
                    }
                });
        })

        let addRespectiveItemName = vit.map(vitIndex =>
            {
                let tempArray = [];
                let item = items.map(itemIndex =>
                    {
                        if(itemIndex.contains.includes(vitIndex))
                        tempArray.push(itemIndex.name);
                    });

                    result.push({[vitIndex]:tempArray});
            });

    return result;
    }
        
        
    //5. Sort items based on number of Vitamins they contain.

    function containVitaminA(items)
    {
    let result = [];
    let vitamins = {};
    let vit = [];

    let availability = items.map(itemIndex => 
        {
            let strArray = itemIndex.contains.split(",");
            let length = strArray.length;

            itemIndex["Vcount"] = length;
            vit.push(length);
            result.push(itemIndex);
        })

        result.sort((item1,item2) =>
        {
            if(item1.Vcount < item2.Vcount)
            {
                return -1;
            }
            else
            {
                return 1;
            }

            return 0;

        })

    return result;
    }

    //NOTE: Do not change the name of this file

