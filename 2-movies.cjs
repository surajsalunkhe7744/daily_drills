const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
}



    //NOTE: For all questions, the returned data must contain all the movie information including its name.

    //Q1. Find all the movies with total earnings more than $500M.
    
    
    function movieEarnings(favouritesMovies){
    let moviesValues = Object.values(favouritesMovies);
    let moviesKeys = Object.keys(favouritesMovies);
    let result = moviesValues.filter((movieIndex,index) => {
        if(Number(movieIndex.totalEarnings.substr(1,movieIndex.totalEarnings.length-2))>500){
            return favouritesMovies[moviesKeys[index]];
        }
    });
    return result;
    }
    
    
    //Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
    
    function movieEarningsAndOscarNominations(favouritesMovies){
    let moviesValues = Object.values(favouritesMovies);
    let moviesKeys = Object.keys(favouritesMovies);
    let result = moviesValues.filter((movieIndex,index) => {
        if(Number(movieIndex.totalEarnings.substr(1,movieIndex.totalEarnings.length-2))>500 && movieIndex.oscarNominations > 3){
            return favouritesMovies[moviesKeys[index]];
        }
    });
    return result;
    }
    
    
    //Q.3 Find all movies of the actor "Leonardo Dicaprio".
    
    function moviesActor(favouritesMovies){
    let moviesValues = Object.values(favouritesMovies);
    let moviesKeys = Object.keys(favouritesMovies);
    let result = moviesValues.filter((movieIndex,index) => {
        if(movieIndex.actors.toString().includes("Leonardo Dicaprio")){
            return favouritesMovies[moviesKeys[index]];
        }
    });
    return result;
    }
    
    
    //Q.4 Sort movies (based on IMDB rating)
    //   if IMDB ratings are same, compare totalEarning as the secondary metric.
        
    function moviesIMDBRating(favouritesMovies){
    let moviesValues = Object.values(favouritesMovies);
    let moviesKeys = Object.keys(favouritesMovies);
    let result = moviesValues.sort((a,b) =>{
        if(a.imdbRating > b.imdbRating){
            return -1;
        }else{
            if(a.totalEarnings.substr(1,a.totalEarnings.length-2) > b.totalEarnings.substr(1,a.totalEarnings.length-2))
            return -1;
            else
            return 1;
        }
    })
    return result;
   }
   
   
    //Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
    //drama > sci-fi > adventure > thriller > crime
    
    function groupingMovies(favouritesMovies){
    let moviesValues = Object.values(favouritesMovies);
    //let moviesKeys = Object.keys(favouritesMovies);
    
    let genreType = moviesValues.map((movie) => {
        return movie.genre.toString();
    });

    let movieGenreType = new Set();

    let movieTypes = genreType.map((genType) =>{
            let temp = genType.split(",");
            movieGenreType.add(temp);
            return temp;
    })
    return movieGenreType;
    }

    //NOTE: Do not change the name of this file
